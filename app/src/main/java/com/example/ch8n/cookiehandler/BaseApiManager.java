package com.example.ch8n.cookiehandler;


import android.content.Context;

import com.example.ch8n.cookiehandler.cookie.AddCookiesInterceptor;
import com.example.ch8n.cookiehandler.cookie.ReceivedCookiesInterceptor;

import java.util.concurrent.TimeUnit;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class BaseApiManager {


//    public final static String BASE_URL = "http://ec2-52-204-144-146.compute-1.amazonaws.com:8000/"; // dev
//    public final static String BASE_URL = "http://ec2-52-204-144-146.compute-1.amazonaws.com:8001/"; // dev 2 old flow

    //public final static String BASE_URL = "http://api.m.getolly.ga/"; // prod server


    private static Retrofit retrofit;

    private static ApiService apiService;

    public BaseApiManager(Context context) {
        createService(context);
    }

    private static void init() {
        apiService = createApi(ApiService.class);
    }

    private static <T> T createApi(Class<T> clazz) {
        return retrofit.create(clazz);
    }

    public static void createService(Context context) {


        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();

        logger.setLevel(HttpLoggingInterceptor.Level.BODY);


        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new ReceivedCookiesInterceptor(context.getApplicationContext()))
                .addInterceptor(new AddCookiesInterceptor(context.getApplicationContext()))
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(logger)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl("http://app.ethrmusic.com:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

//        retrofit = new Retrofit.Builder()
//                .baseUrl(new BaseURL().getUrl())
//                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
//                .client(okHttpClient)
//                .build();
        init();
    }

    public ApiService getApiService() {
        return apiService;
    }

}
