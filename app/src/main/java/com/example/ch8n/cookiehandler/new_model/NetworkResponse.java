
package com.example.ch8n.cookiehandler.new_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NetworkResponse {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("headers")
    @Expose
    private Headers_ headers;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("protocol")
    @Expose
    private String protocol;
    @SerializedName("receivedResponseAtMillis")
    @Expose
    private Integer receivedResponseAtMillis;
    @SerializedName("request")
    @Expose
    private Request request;
    @SerializedName("sentRequestAtMillis")
    @Expose
    private Integer sentRequestAtMillis;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Headers_ getHeaders() {
        return headers;
    }

    public void setHeaders(Headers_ headers) {
        this.headers = headers;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Integer getReceivedResponseAtMillis() {
        return receivedResponseAtMillis;
    }

    public void setReceivedResponseAtMillis(Integer receivedResponseAtMillis) {
        this.receivedResponseAtMillis = receivedResponseAtMillis;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Integer getSentRequestAtMillis() {
        return sentRequestAtMillis;
    }

    public void setSentRequestAtMillis(Integer sentRequestAtMillis) {
        this.sentRequestAtMillis = sentRequestAtMillis;
    }

}
