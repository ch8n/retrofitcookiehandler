
package com.example.ch8n.cookiehandler.new_model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Headers_____ {

    @SerializedName("namesAndValues")
    @Expose
    private List<Object> namesAndValues = null;

    public List<Object> getNamesAndValues() {
        return namesAndValues;
    }

    public void setNamesAndValues(List<Object> namesAndValues) {
        this.namesAndValues = namesAndValues;
    }

}
