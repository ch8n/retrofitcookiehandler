
package com.example.ch8n.cookiehandler.new_model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Headers {

    @SerializedName("namesAndValues")
    @Expose
    private List<String> namesAndValues = null;

    public List<String> getNamesAndValues() {
        return namesAndValues;
    }

    public void setNamesAndValues(List<String> namesAndValues) {
        this.namesAndValues = namesAndValues;
    }

}
