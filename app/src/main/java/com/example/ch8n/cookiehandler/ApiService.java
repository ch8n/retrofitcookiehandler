package com.example.ch8n.cookiehandler;


import com.example.ch8n.cookiehandler.new_model.Response;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by ch8n on 28/12/17.
 */

public interface ApiService {

    @FormUrlEncoded
    @POST("ws/login")
    Call<Response> getLoginDetails(@Field("username") String username, @Field("password") String password);
}
