
package com.example.ch8n.cookiehandler.new_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tag_ {

    @SerializedName("body")
    @Expose
    private Body____ body;
    @SerializedName("headers")
    @Expose
    private Headers_____ headers;
    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("url")
    @Expose
    private Url__ url;

    public Body____ getBody() {
        return body;
    }

    public void setBody(Body____ body) {
        this.body = body;
    }

    public Headers_____ getHeaders() {
        return headers;
    }

    public void setHeaders(Headers_____ headers) {
        this.headers = headers;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Url__ getUrl() {
        return url;
    }

    public void setUrl(Url__ url) {
        this.url = url;
    }

}
