
package com.example.ch8n.cookiehandler.new_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tag {

    @SerializedName("body")
    @Expose
    private Body__ body;
    @SerializedName("headers")
    @Expose
    private Headers___ headers;
    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("url")
    @Expose
    private Url url;

    public Body__ getBody() {
        return body;
    }

    public void setBody(Body__ body) {
        this.body = body;
    }

    public Headers___ getHeaders() {
        return headers;
    }

    public void setHeaders(Headers___ headers) {
        this.headers = headers;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }

}
