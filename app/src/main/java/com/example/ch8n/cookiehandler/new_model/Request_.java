
package com.example.ch8n.cookiehandler.new_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Request_ {

    @SerializedName("body")
    @Expose
    private Body___ body;
    @SerializedName("headers")
    @Expose
    private Headers____ headers;
    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("tag")
    @Expose
    private Tag_ tag;
    @SerializedName("url")
    @Expose
    private Url___ url;

    public Body___ getBody() {
        return body;
    }

    public void setBody(Body___ body) {
        this.body = body;
    }

    public Headers____ getHeaders() {
        return headers;
    }

    public void setHeaders(Headers____ headers) {
        this.headers = headers;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Tag_ getTag() {
        return tag;
    }

    public void setTag(Tag_ tag) {
        this.tag = tag;
    }

    public Url___ getUrl() {
        return url;
    }

    public void setUrl(Url___ url) {
        this.url = url;
    }

}
