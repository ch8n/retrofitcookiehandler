package com.example.ch8n.cookiehandler;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


import com.example.ch8n.cookiehandler.new_model.Response;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.HashSet;

import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity implements Callback<Response> {


    private BaseApiManager apiManager;
    private Call<Response> response;
    private retrofit2.Response<Response> responsemodel;
    private TextView tv_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tv_content = findViewById(R.id.tv_content);

        apiManager = new BaseApiManager(this);

        apiManager.getApiService().getLoginDetails("stsaini", "password").enqueue(this);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tv_content.setText(new Gson().toJson(responsemodel.raw().request().headers().get("Cookie")));

                //OR
                HashSet<String> preferences = (HashSet) PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getStringSet("PREF_COOKIES", new HashSet<String>());

                tv_content.setText(new Gson().toJson(preferences));


            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
        responsemodel = response;
    }

    @Override
    public void onFailure(Call<Response> call, Throwable t) {

    }
}
