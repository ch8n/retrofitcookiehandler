
package com.example.ch8n.cookiehandler.new_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RawResponse {

    @SerializedName("body")
    @Expose
    private Body body;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("headers")
    @Expose
    private Headers headers;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("networkResponse")
    @Expose
    private NetworkResponse networkResponse;
    @SerializedName("protocol")
    @Expose
    private String protocol;
    @SerializedName("receivedResponseAtMillis")
    @Expose
    private Integer receivedResponseAtMillis;
    @SerializedName("request")
    @Expose
    private Request_ request;
    @SerializedName("sentRequestAtMillis")
    @Expose
    private Integer sentRequestAtMillis;

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public NetworkResponse getNetworkResponse() {
        return networkResponse;
    }

    public void setNetworkResponse(NetworkResponse networkResponse) {
        this.networkResponse = networkResponse;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Integer getReceivedResponseAtMillis() {
        return receivedResponseAtMillis;
    }

    public void setReceivedResponseAtMillis(Integer receivedResponseAtMillis) {
        this.receivedResponseAtMillis = receivedResponseAtMillis;
    }

    public Request_ getRequest() {
        return request;
    }

    public void setRequest(Request_ request) {
        this.request = request;
    }

    public Integer getSentRequestAtMillis() {
        return sentRequestAtMillis;
    }

    public void setSentRequestAtMillis(Integer sentRequestAtMillis) {
        this.sentRequestAtMillis = sentRequestAtMillis;
    }

}
