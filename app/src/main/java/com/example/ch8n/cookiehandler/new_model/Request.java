
package com.example.ch8n.cookiehandler.new_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Request {

    @SerializedName("body")
    @Expose
    private Body_ body;
    @SerializedName("cacheControl")
    @Expose
    private CacheControl cacheControl;
    @SerializedName("headers")
    @Expose
    private Headers__ headers;
    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("tag")
    @Expose
    private Tag tag;
    @SerializedName("url")
    @Expose
    private Url_ url;

    public Body_ getBody() {
        return body;
    }

    public void setBody(Body_ body) {
        this.body = body;
    }

    public CacheControl getCacheControl() {
        return cacheControl;
    }

    public void setCacheControl(CacheControl cacheControl) {
        this.cacheControl = cacheControl;
    }

    public Headers__ getHeaders() {
        return headers;
    }

    public void setHeaders(Headers__ headers) {
        this.headers = headers;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public Url_ getUrl() {
        return url;
    }

    public void setUrl(Url_ url) {
        this.url = url;
    }

}
