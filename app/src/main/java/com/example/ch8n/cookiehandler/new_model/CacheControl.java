
package com.example.ch8n.cookiehandler.new_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CacheControl {

    @SerializedName("immutable")
    @Expose
    private Boolean immutable;
    @SerializedName("isPrivate")
    @Expose
    private Boolean isPrivate;
    @SerializedName("isPublic")
    @Expose
    private Boolean isPublic;
    @SerializedName("maxAgeSeconds")
    @Expose
    private Integer maxAgeSeconds;
    @SerializedName("maxStaleSeconds")
    @Expose
    private Integer maxStaleSeconds;
    @SerializedName("minFreshSeconds")
    @Expose
    private Integer minFreshSeconds;
    @SerializedName("mustRevalidate")
    @Expose
    private Boolean mustRevalidate;
    @SerializedName("noCache")
    @Expose
    private Boolean noCache;
    @SerializedName("noStore")
    @Expose
    private Boolean noStore;
    @SerializedName("noTransform")
    @Expose
    private Boolean noTransform;
    @SerializedName("onlyIfCached")
    @Expose
    private Boolean onlyIfCached;
    @SerializedName("sMaxAgeSeconds")
    @Expose
    private Integer sMaxAgeSeconds;

    public Boolean getImmutable() {
        return immutable;
    }

    public void setImmutable(Boolean immutable) {
        this.immutable = immutable;
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public Integer getMaxAgeSeconds() {
        return maxAgeSeconds;
    }

    public void setMaxAgeSeconds(Integer maxAgeSeconds) {
        this.maxAgeSeconds = maxAgeSeconds;
    }

    public Integer getMaxStaleSeconds() {
        return maxStaleSeconds;
    }

    public void setMaxStaleSeconds(Integer maxStaleSeconds) {
        this.maxStaleSeconds = maxStaleSeconds;
    }

    public Integer getMinFreshSeconds() {
        return minFreshSeconds;
    }

    public void setMinFreshSeconds(Integer minFreshSeconds) {
        this.minFreshSeconds = minFreshSeconds;
    }

    public Boolean getMustRevalidate() {
        return mustRevalidate;
    }

    public void setMustRevalidate(Boolean mustRevalidate) {
        this.mustRevalidate = mustRevalidate;
    }

    public Boolean getNoCache() {
        return noCache;
    }

    public void setNoCache(Boolean noCache) {
        this.noCache = noCache;
    }

    public Boolean getNoStore() {
        return noStore;
    }

    public void setNoStore(Boolean noStore) {
        this.noStore = noStore;
    }

    public Boolean getNoTransform() {
        return noTransform;
    }

    public void setNoTransform(Boolean noTransform) {
        this.noTransform = noTransform;
    }

    public Boolean getOnlyIfCached() {
        return onlyIfCached;
    }

    public void setOnlyIfCached(Boolean onlyIfCached) {
        this.onlyIfCached = onlyIfCached;
    }

    public Integer getSMaxAgeSeconds() {
        return sMaxAgeSeconds;
    }

    public void setSMaxAgeSeconds(Integer sMaxAgeSeconds) {
        this.sMaxAgeSeconds = sMaxAgeSeconds;
    }

}
