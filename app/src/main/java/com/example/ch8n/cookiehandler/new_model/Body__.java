
package com.example.ch8n.cookiehandler.new_model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Body__ {

    @SerializedName("encodedNames")
    @Expose
    private List<String> encodedNames = null;
    @SerializedName("encodedValues")
    @Expose
    private List<String> encodedValues = null;

    public List<String> getEncodedNames() {
        return encodedNames;
    }

    public void setEncodedNames(List<String> encodedNames) {
        this.encodedNames = encodedNames;
    }

    public List<String> getEncodedValues() {
        return encodedValues;
    }

    public void setEncodedValues(List<String> encodedValues) {
        this.encodedValues = encodedValues;
    }

}
