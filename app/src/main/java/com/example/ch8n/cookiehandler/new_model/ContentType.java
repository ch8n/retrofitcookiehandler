
package com.example.ch8n.cookiehandler.new_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentType {

    @SerializedName("charset")
    @Expose
    private String charset;
    @SerializedName("mediaType")
    @Expose
    private String mediaType;
    @SerializedName("subtype")
    @Expose
    private String subtype;
    @SerializedName("type")
    @Expose
    private String type;

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
